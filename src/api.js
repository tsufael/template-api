const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");
const UserMiddleware = require("./middlewares/User.middleware");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  api.post("/users", UserMiddleware.validateUserBody, async (req, res) => {
    const user = {
      user: {
        id: uuid(),
        name: req.body.name,
        email: req.body.email,
      }
    }

    const database = mongoClient.db('main');
    const users = database.collection('users');
    const userFound = await users.findOne({
      email: user.email
    });
    if (userFound) {
      return res.status(403).send({ error: "user already exists" });
    }

    const eventToPublish = {
      eventType: "UserCreated",
      entityId: user.id,
      entityAggregate: {
        name: user.name,
        email: user.email,
        password: user.password,
      }
    }
    
    stanConn.publish('users', JSON.stringify(eventToPublish), (err, guid) => {
      if (err) {
        console.log('publish failed: ' + err);
      } else {
        console.log('published message with guid: ' + guid);
      }
    });

    return res.status(201).send(user);
  });

  api.delete("/users/:id", async (req, res) => {

    const id = req.params.id;
    const eventToPublish = {
      eventType: "UserDeleted",
      entityId: id,
      entityAggregate: {}
    }
    
    const authentication = req.headers.authentication;
    if(!authentication) {
      return res.status(401).send({ error: "Access token not found" });
    }
    const token = authentication.split(" ")[1];
    const decoded = jwt.verify(token, secret);
    if (decoded.id !== id) {
      return res.status(403).send({ error: "Access Token did not match User ID" });
    }

    const database = mongoClient.db('main');
    const users = database.collection('users');
    const userFound = await users.findOne({
      id: id 
    });
    
    stanConn.publish('users', JSON.stringify(eventToPublish));
    return res.status(200).send({ id: id });
  });

  return api;
};
